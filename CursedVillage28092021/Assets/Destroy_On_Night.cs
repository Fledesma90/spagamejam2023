using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy_On_Night : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.GetString("DayTime");
        if (PlayerPrefs.GetString("DayTime") == "night")
        {
            Destroy(gameObject);
        }
    }
}
