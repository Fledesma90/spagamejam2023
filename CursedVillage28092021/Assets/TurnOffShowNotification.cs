using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffShowNotification : MonoBehaviour
{
    //ESTE ESCRIPT NOS SERVIRÁ PARA ARREGLAR BUGS DE LAS NOTIFICACIONES DE PISTAS
    [SerializeField] NotesHandler notesHandler;
    public GameObject cerrarLibro;
    public GameObject abrirLibro;
    private void OnEnable() {
        notesHandler.canShowNotification = false;
        Instantiate(abrirLibro);
    }
    private void OnDisable() {
        notesHandler.canShowNotification = true;
        notesHandler.cluesObtained=notesHandler.currentClues;
        Instantiate(cerrarLibro);
    }
}
