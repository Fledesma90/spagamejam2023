using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeCinematic : MonoBehaviour
{
    [SerializeField] CanvasGroup canvasGroup;
    [SerializeField] float time;
    // Start is called before the first frame update
    void OnEnable()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        StartCoroutine(FadeIn());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator FadeIn() {
        while (canvasGroup.alpha > 0) {
            canvasGroup.alpha -= Time.deltaTime / time;
            yield return null;
        }
        if (canvasGroup.alpha == 0) {
            gameObject.SetActive(false);
        }
    }
}
