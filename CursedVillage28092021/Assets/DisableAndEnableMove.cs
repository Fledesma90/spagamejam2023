using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableAndEnableMove : MonoBehaviour
{
    PlayerController playerController;

    private void OnEnable() {
        playerController = FindObjectOfType<PlayerController>();
        playerController.enabled = false;
    }

    private void OnDisable() {
        playerController.enabled = true;
    }
}
