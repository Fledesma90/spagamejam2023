using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateText : MonoBehaviour
{
    public GameObject EspObject;
    public GameObject EngObject;
    // Start is called before the first frame update
    void Update()
    {
        if (PlayerPrefs.GetString("Language")== "Spanish") {
            EngObject.SetActive(false);
            EspObject.SetActive(true);
        }
        if (PlayerPrefs.GetString("Language") == "English") {
            EngObject.SetActive(true);
            EspObject.SetActive(false);
        }
    }

}
