using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class changeSceneToAldea : MonoBehaviour
{
    public Fade fader;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(fader.GetComponent<Fade>().FadeOut());
        Invoke("ChangeToAldea", 4f);
    }

    void ChangeToAldea() {
        //despues de la cinemática ponemos la variable en día
        PlayerPrefs.SetString("DayTime", "day");
        SceneManager.LoadScene("Aldea");
    }
}
