using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MusicManager : MonoBehaviour {
    #region ######VARIABLES
    public AudioClip mainMenuMusic;
    public AudioClip dayMusic;
    public AudioClip nightMusic;
    public AudioClip indoorMusic;
    //TODO AUDIOCLIP musica de la primera cinematica, del nivel outdoor, nivel underground, final boss
    AudioSource audioSource;
    private static MusicManager musicManager;
    //numero que va a evitar que las funciones de reproducir musica se reproduzcan dos veces
    //ya que comprobaremos en los scripts de cambio de canción el valor de esta variable
    public int trackPlaying;
    #endregion
    #region #######METHODS
    private void Awake() {
        if (musicManager == null) {
            DontDestroyOnLoad(this);
            //esta linea de código hace que no se te duplique el objeto 
            musicManager = this;
        } else {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start() {
        audioSource = GetComponent<AudioSource>();
        //if (SceneManager.GetActiveScene().name == "MainMenu") {
        //    audioSource.clip = mainMenuMusic;
        //    audioSource.Play();
        //}
    }
    public void MainMenuMusic() {
        audioSource.clip = mainMenuMusic;
        audioSource.Play();
        trackPlaying = 1;
    }
    public void DayMusic() {
        audioSource.clip = dayMusic;
        audioSource.Play();
        trackPlaying = 2;
    }
    public void NightMusic() {
        audioSource.clip = nightMusic;
        audioSource.Play();
        trackPlaying = 3;
    }
    public void IndoorMusic() {
        audioSource.clip = indoorMusic;
        audioSource.Play();
        trackPlaying = 4;
    }
    #endregion

}
