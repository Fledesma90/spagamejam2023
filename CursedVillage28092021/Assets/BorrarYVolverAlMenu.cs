using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class BorrarYVolverAlMenu : MonoBehaviour
{
    //Unity el objeto FadeImage a esto
    [SerializeField] GameObject fade;
    public void MenuPrincipal() {
        PlayerPrefs.DeleteAll();
        if (fade.activeInHierarchy == true)
        {
            StartCoroutine(fade.GetComponent<Fade>().FadeOut());
            Invoke("LoadMenu", 1);
            fade.SetActive(true);
        }
        else
        {
            StartCoroutine(fade.GetComponent<Fade>().FadeOut());
            Invoke("LoadMenu", 1);
        }

    }

    void LoadMenu() {
        SceneManager.LoadScene("MainMenu");
    }
}
