using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class TextAnim : MonoBehaviour {
    [SerializeField] TextMeshProUGUI textMPRO;

    public string[] stringArray;
    public float timeBetweenCharacters;
    public float timeBetweenWords;
    int i = 0;
    bool canGoFast;
    [SerializeField] NPCdialogue nPCdialogue;
 
    // Start is called before the first frame update
    void Start() {
        canGoFast = true;
        EndCheck();
    }
    private void Update() {
        if (Input.GetButtonDown("Jump") && canGoFast==true) {
            timeBetweenCharacters = timeBetweenCharacters * -10;
            timeBetweenWords = timeBetweenWords * -10;
            canGoFast = false;
        }
    }
    void EndCheck() {
        if (i <= stringArray.Length - 1) {
            textMPRO.text = stringArray[i];
            StartCoroutine(TextVisible());
        }
    }
    private IEnumerator TextVisible() {
        textMPRO.ForceMeshUpdate();
        int totalVisibleCharacters = textMPRO.textInfo.characterCount;
        int counter = 0;
        EnableDialoguesSlowInCinematic();
        DialoguesSlowInWorld();
        while (true) {
            int visibleCount = counter % (totalVisibleCharacters + 1);
            textMPRO.maxVisibleCharacters = visibleCount;

            if (visibleCount >= totalVisibleCharacters) {
                i += 1;
                Invoke("EndCheck", timeBetweenWords);
                //AQUÍ SE COMPRUEBA QUE SE HAYA TERMINADO DE LEER LOS DIÁLOGOS
                DialoguesEnableToPassOnCinematic();
                EnablePassDialogue();
                break;
            }

            counter += 1;
            yield return new WaitForSeconds(timeBetweenCharacters);
        }
    }

    void EnableDialoguesSlowInCinematic() {
        if (SceneManager.GetActiveScene().name == "Cinematic") {
            cinematicHandler cinematic = FindObjectOfType<cinematicHandler>();
            cinematic.canPassDialogue = false;
        }
    }

    void DialoguesEnableToPassOnCinematic() {
        if (SceneManager.GetActiveScene().name=="Cinematic") {
            cinematicHandler cinematic;
            cinematic = FindObjectOfType<cinematicHandler>();
            cinematic.canPassDialogue = true;
        }
    }

    void DialoguesSlowInWorld() {
        if (SceneManager.GetActiveScene().name != "Cinematic") {

            nPCdialogue.canPassDialogue = false;

        }
    }
    void EnablePassDialogue() {
        if (SceneManager.GetActiveScene().name != "Cinematic") {

            nPCdialogue.canPassDialogue = true;

        }
    }
}
