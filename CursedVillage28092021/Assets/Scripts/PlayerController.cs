using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerController : MonoBehaviour
{
    public float speed;
    public float groundDist;

    public LayerMask terrainLayer;
    public Rigidbody rb;
    public SpriteRenderer sr;

    public Animator animator;
    public GameObject pasosVFXIzq;
    public GameObject pasosVFXDrcha;

    PlayerCameraSwitcher playerCameraSwitcher;
    private void Awake() {
        playerCameraSwitcher = gameObject.GetComponent<PlayerCameraSwitcher>();
        if (SceneManager.GetActiveScene().name=="Aldea") {
            playerCameraSwitcher.enabled = true;
        } else {
            playerCameraSwitcher.enabled = false;
        }
    }
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (rb.velocity.magnitude <= 0)
        {
            animator.SetBool("isWalking", false);
            pasosVFXIzq.SetActive(false);
            pasosVFXDrcha.SetActive(false);
        }
        else
        {
            animator.SetBool("isWalking", true);

            pasosVFXIzq.SetActive(true);
            pasosVFXDrcha.SetActive(true);

            if (sr.flipX == true)
            {
                pasosVFXDrcha.SetActive(true);
                pasosVFXIzq.SetActive(false);
            }
            else
            {
                pasosVFXDrcha.SetActive(false);
                pasosVFXIzq.SetActive(true);
            }
        }

        RaycastHit hit;
        Vector3 castPos = transform.position;

        castPos.y += 1;

        if (Physics.Raycast(castPos, -transform.up, out hit, Mathf.Infinity, terrainLayer))
        {
            if (hit.collider != null)
            {
                Vector3 movePos = transform.position;
                movePos.y = hit.point.y + groundDist;
                transform.position = movePos;
            }
        }

        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        Vector3 moveDir = new Vector3(-x, 0, -y);
        rb.velocity = moveDir * speed;

        if (x != 0 && x > 0)
        {
            sr.flipX = true;
        }
        else if (x !=0 && x < 0)
        {
            sr.flipX = false;
        }
    }
    private void OnDisable() 
    {
        animator.SetBool("isWalking", false);
        rb.velocity = Vector3.zero;
        pasosVFXIzq.SetActive(false);
        pasosVFXDrcha.SetActive(false);
    }
    private void OnCollisionEnter(Collision collision)
    {
        animator.SetBool("isWalking", false);
        rb.velocity = Vector3.zero;
        pasosVFXIzq.SetActive(false);
        pasosVFXDrcha.SetActive(false);
    }
    
}
