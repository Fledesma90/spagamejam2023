using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatePressToTalk : MonoBehaviour
{
    #region VARIABLES_______________________________________________________________________________
    [SerializeField] float minDistance;
    private Transform player;
    public GameObject text;
    //referencia al script npcdialogue para que solo podamos hablar si la booleana playerin es true
    NPCdialogue nPCdialogue;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        nPCdialogue = GetComponent<NPCdialogue>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        LookAtPlayerEng();
    }
    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, minDistance);
    }
    private void LookAtPlayerEng() {
        if (Vector3.Distance(transform.position, player.position) < minDistance ) {
            nPCdialogue.playerIn = true;
            text.SetActive(true);
        } else {
            nPCdialogue.playerIn = false;
            text.SetActive(false);
            return;
        }
    }
}
