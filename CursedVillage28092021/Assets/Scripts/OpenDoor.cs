using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class OpenDoor : MonoBehaviour
{
    //Unity el objeto FadeImage a esto
    [SerializeField] GameObject fade;
    //En el inspector poner el nombre exacto de la escena a cargar
    [SerializeField] string SceneToLoad;
    int isIndoor;
    [SerializeField] string doorToAppear;
    [SerializeField] GameObject barGate;
    [SerializeField] GameObject casaFresiGate;
    [SerializeField] GameObject player;
    private void Awake() {
        if (gameObject.name== "CasaFresi") {
            casaFresiGate = GameObject.Find("casaFresiGate");
        }
        if (gameObject.name == "Bar") {
            casaFresiGate = GameObject.Find("barGate");
        }
        //Dependiendo del valor de la variable playerprefs DoorToAppear
        //el player apareerá en un lugar u otro
        PlayerPrefs.GetString("DoorToAppear");
        if (PlayerPrefs.GetString("DoorToAppear")=="Bar") {
            player.transform.position = barGate.transform.position;
        }

        if (PlayerPrefs.GetString("DoorToAppear") == "CasaFresi") {
            player.transform.position = casaFresiGate.transform.position;
        }
    }
    private void Update() {
        isIndoor = PlayerPrefs.GetInt("IsIndoor");

        


        //verificamos que el objeto sea el trigger de la puerta del sótano para
        //comprobar que tenga la llave 
        if (gameObject.name=="SotanoTrigger") {
            PlayerPrefs.GetInt("IsKey");
            if (PlayerPrefs.GetInt("IsKey")!=1) {
                gameObject.GetComponent<BoxCollider>().enabled = false;
            } else {
                gameObject.GetComponent<BoxCollider>().enabled = true;
            }
        }
    }
    //Comprobamos con qué trigger chocamos y el nombre de este mismo objeto
    //dependiendo del objeto entraremos a un nivel u otro
    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<PlayerController>()) {
            if (doorToAppear!="") {
                PlayerPrefs.SetString("DoorToAppear", doorToAppear);
            } else {
                PlayerPrefs.SetString("DoorToAppear", "");
            }
            LoadNextSceneWorld();
            //Para que no clipee la musica dentro de casa el objeto para salir tiene que llamarse
            //EXIT
            if (gameObject.name=="Exit") {
                PlayerPrefs.SetInt("IsIndoor", 0);
            } else {
                PlayerPrefs.SetInt("IsIndoor", 1);
            }
            //desactivamos el componente para que deje de andar
            player.GetComponent<PlayerController>().enabled = false;
            player.GetComponent<Animator>().SetBool("isWalking", false);
           
        }
    }
    /// <summary>
    /// Method to load level 1
    /// </summary>
    public void LoadNextSceneWorld() {
        fade.SetActive(true);
        StartCoroutine(fade.GetComponent<Fade>().FadeOut());
        
        Invoke("LoadNextScene", 3f);
    }
    public void LoadNextScene() {
        switch (SceneToLoad) {
            case "Bar":
                SceneManager.LoadScene("Bar");
                break;
            case "Aldea":
                SceneManager.LoadScene("Aldea");
                break;
            case "CasaPepiYFresi":
                SceneManager.LoadScene("CasaPepiYFresi");
                break;
            case "SotanoCasaPepiYFresi":
                SceneManager.LoadScene("SotanoCasaPepiYFresi");
                break;
        }
    }

}
