using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class indoorMusic : MonoBehaviour
{
    public MusicManager musicManager;
    int isIndoor;
    // Start is called before the first frame update
    void Start() {
        musicManager = FindObjectOfType<MusicManager>();
        isIndoor = PlayerPrefs.GetInt("IsIndoor");
        if (isIndoor==1) {
            musicManager.trackPlaying = 4;
            //dado que no podemos acceder al método de musicManager desde este script(razones desconocidas)
            //
            if (musicManager.trackPlaying == 4) {
                AudioSource audioSource = musicManager.GetComponent<AudioSource>();
                audioSource.clip = musicManager.indoorMusic;
                audioSource.Play();
            }
        }
    }
}
