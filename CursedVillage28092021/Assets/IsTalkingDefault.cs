using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsTalkingDefault : MonoBehaviour
{
    [SerializeField] NPCdialogue nPCdialogue;

    //private void OnEnable() {
    //    nPCdialogue.isTalking = true;
    //}
    private void Start() {

        nPCdialogue.isTalking = false;
        
    }
    private void OnDisable() {
        nPCdialogue.canPassDialogue = true;
    }
    //private void OnDisable() {
    //    nPCdialogue.isTalking = false;
    //}

}
