using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript : MonoBehaviour
{
    public GameObject exit;
    private void Start() {
        //desactivamos la salida para que el jugador no pueda salir hasta coger la llave
        exit.SetActive(false);
        //Si ya tenemos la llave destruimos este objeto para no volverlo a ver en la escena
        PlayerPrefs.GetInt("IsKey");
        if (PlayerPrefs.GetInt("IsKey")==1) {
            Destroy(gameObject);
        }
    }
    //al chocar con el player ponemos la variable playerprefs IsKey a 1
    //y destrumos la llave
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.GetComponent<PlayerController>()) {
            PlayerPrefs.SetInt("IsKey", 1);
            //activamos la salida
            exit.SetActive(true);
        }
        Destroy(gameObject);
        
    }
}
