
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCdialogue : MonoBehaviour
{
    //bool para cuando no se haya hablado nunca con ese NPC
    [SerializeField] bool isDialogue1;
    //bool para cuando se haya hablado antes con ese NPC
    [SerializeField] bool isDefault1;
    //numero de la lista de diálogos
    [SerializeField] int dialogueNumber;
    //bool para activar temporizador
    public bool timerOn = false;
    //temporizador que dependerá de la booleana 
    public float timer = 4f;
    //bool para comprobar que el player está dentro
    public bool playerIn;
    //Parent del dialogo primero
    public GameObject dialogueParent;
    //Referencia al diálogo por defecto
    public GameObject defaultDialogue;
    //lista de diálogos
    public List<GameObject> dialogues = new List<GameObject>();
    PlayerController playerController;
    [SerializeField] float timeToLastDefaultDialogue;
    //esta variable nos servirá para sumar a la variable PlayerPrefs.("SpeakNumber")
    int currentNpcSpoken;
    int currentNpcSpokenToAdd = 1;

    [SerializeField] GameObject dialogoJuzgar;
    public bool canInteract;
    //estableceremos estas variables true o false dependiendo del objeto activo
    public bool isTalking;
    public bool isJudging;
    //Bool que está enlazada al script de animación de texto 
    public bool canPassDialogue;
    //referencia a las notas para poder enseñar las notas o no
    NotesHandler notes;
    npcCounter npcCounter;
    // Start is called before the first frame update
    void Start()
    {
        canPassDialogue = true;
        //recogemos la referencia de notes
        notes = FindObjectOfType<NotesHandler>();
        npcCounter = FindObjectOfType<npcCounter>();
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        CheckIfSpokenFirst();
        if (isDialogue1 && gameObject.name == "Picota")
        {
            //TODO dependiendo del momento el diálogo será uno u otro
            //Nos fijaremos en el tag del objeto SpaDialogue, por ejemplo Picota lleva dialogue1
            dialogueParent = GameObject.FindGameObjectWithTag("dialogue1");
            foreach (Transform item in dialogueParent.transform)
            {
                //añadimos los diálogos como hijos de dialogueParent
                dialogues.Add(item.gameObject);
            }
        }
        if (isDialogue1 && gameObject.name == "Pina")
        {
            //TODO dependiendo del momento el diálogo será uno u otro
            //Nos fijaremos en el tag del objeto SpaDialogue, por ejemplo Picota lleva dialogue1
            dialogueParent = GameObject.FindGameObjectWithTag("dialoguepina1");
            foreach (Transform item in dialogueParent.transform)
            {
                //añadimos los diálogos como hijos de dialogueParent
                dialogues.Add(item.gameObject);
            }
        }
        //ESTO ES EXTRA
        if (isDialogue1 && gameObject.name == "Fresi")
        {
            //TODO dependiendo del momento el diálogo será uno u otro
            //Nos fijaremos en el tag del objeto SpaDialogue, por ejemplo Picota lleva dialogue1
            dialogueParent = GameObject.FindGameObjectWithTag("dialoguefresa1");
            foreach (Transform item in dialogueParent.transform)
            {
                //añadimos los diálogos como hijos de dialogueParent
                dialogues.Add(item.gameObject);
            }
        }
        if (isDialogue1 && gameObject.name == "Manzana")
        {
            //TODO dependiendo del momento el diálogo será uno u otro
            //Nos fijaremos en el tag del objeto SpaDialogue, por ejemplo Picota lleva dialogue1
            dialogueParent = GameObject.FindGameObjectWithTag("dialoguemanzana1");
            foreach (Transform item in dialogueParent.transform)
            {
                //añadimos los diálogos como hijos de dialogueParent
                dialogues.Add(item.gameObject);
            }
        }
        if (isDialogue1 && gameObject.name == "Uva")
        {
            //TODO dependiendo del momento el diálogo será uno u otro
            //Nos fijaremos en el tag del objeto SpaDialogue, por ejemplo Picota lleva dialogue1
            dialogueParent = GameObject.FindGameObjectWithTag("dialogueuvas1");
            foreach (Transform item in dialogueParent.transform)
            {
                //añadimos los diálogos como hijos de dialogueParent
                dialogues.Add(item.gameObject);
            }
        }

        if (isDialogue1 && gameObject.name == "Peach")
        {
            //TODO dependiendo del momento el diálogo será uno u otro
            //Nos fijaremos en el tag del objeto SpaDialogue, por ejemplo Picota lleva dialogue1
            dialogueParent = GameObject.FindGameObjectWithTag("dialoguepeach1");
            foreach (Transform item in dialogueParent.transform)
            {
                //añadimos los diálogos como hijos de dialogueParent
                dialogues.Add(item.gameObject);
            }
        }
        if (isDialogue1 && gameObject.name == "Pico_Noche")
        {
            //TODO dependiendo del momento el diálogo será uno u otro
            //Nos fijaremos en el tag del objeto SpaDialogue, por ejemplo Picota lleva dialogue1
            dialogueParent = GameObject.FindGameObjectWithTag("dialoguepiconoche1");
            foreach (Transform item in dialogueParent.transform)
            {
                //añadimos los diálogos como hijos de dialogueParent
                dialogues.Add(item.gameObject);
            }
        }
        if (isDialogue1 && gameObject.name == "Ta_Noche") {
            //TODO dependiendo del momento el diálogo será uno u otro
            //Nos fijaremos en el tag del objeto SpaDialogue, por ejemplo Picota lleva dialogue1
            dialogueParent = GameObject.FindGameObjectWithTag("dialoguetanoche1");
            foreach (Transform item in dialogueParent.transform) {
                //añadimos los diálogos como hijos de dialogueParent
                dialogues.Add(item.gameObject);
            }
        }
        if (isDialogue1 && gameObject.name == "Piña_Noche")
        {
            //TODO dependiendo del momento el diálogo será uno u otro
            //Nos fijaremos en el tag del objeto SpaDialogue, por ejemplo Picota lleva dialogue1
            dialogueParent = GameObject.FindGameObjectWithTag("dialoguepiñanoche1");
            foreach (Transform item in dialogueParent.transform)
            {
                //añadimos los diálogos como hijos de dialogueParent
                dialogues.Add(item.gameObject);
            }
        }
        if (isDialogue1 && gameObject.name == "Fresi_Noche")
        {
            //TODO dependiendo del momento el diálogo será uno u otro
            //Nos fijaremos en el tag del objeto SpaDialogue, por ejemplo Picota lleva dialogue1
            dialogueParent = GameObject.FindGameObjectWithTag("dialoguefresinoche1");
            foreach (Transform item in dialogueParent.transform)
            {
                //añadimos los diálogos como hijos de dialogueParent
                dialogues.Add(item.gameObject);
            }
        }
        if (isDialogue1 && gameObject.name == "Uva_Noche")
        {
            //TODO dependiendo del momento el diálogo será uno u otro
            //Nos fijaremos en el tag del objeto SpaDialogue, por ejemplo Picota lleva dialogue1
            dialogueParent = GameObject.FindGameObjectWithTag("dialogueuvanoche1");
            foreach (Transform item in dialogueParent.transform)
            {
                //añadimos los diálogos como hijos de dialogueParent
                dialogues.Add(item.gameObject);
            }
        }
        if (isDialogue1 && gameObject.name == "Peach_Noche")
        {
            //TODO dependiendo del momento el diálogo será uno u otro
            //Nos fijaremos en el tag del objeto SpaDialogue, por ejemplo Picota lleva dialogue1
            dialogueParent = GameObject.FindGameObjectWithTag("dialoguepeachnoche1");
            foreach (Transform item in dialogueParent.transform)
            {
                //añadimos los diálogos como hijos de dialogueParent
                dialogues.Add(item.gameObject);
            }
        }
    }

    // Update is called once per frame
    void Update() {
        if (playerIn == true && Input.GetButtonDown("Jump") && isJudging == false && canInteract == true
            && canPassDialogue==true && npcCounter.disableOtherDialoguesWhileCinematic==false) {
            Conversation();
        }

        if (playerIn == true && Input.GetKeyDown(KeyCode.Z) && isTalking == false && canInteract == true &&

            npcCounter.disableOtherDialoguesWhileCinematic == false) {
            Juzgar();
        }
        if (timerOn) {
      
            timer -= Time.deltaTime;
            if ((timer <= 0)) {
                CheckIfSpoken();
                timerOn = false;
                timer = 4;

                //dialogueParent.SetActive(false);
            }
        }

        //CanShowNotesMethod();

    }
    /// <summary>
    /// Método para poder ver las notas o no 
    /// </summary>
    //private void CanShowNotesMethod() {
    //    if (isTalking || isJudging) {
    //        notes.canShowNotes = false;
    //    } else if (!isTalking || !isJudging) {
    //        notes.canShowNotes = true;
    //    }
    //}

    void Conversation()
    {
        if (isDialogue1 && timerOn == false)
        {
            FirstDialogue();
        }
        if (isDefault1 && timerOn == false)
        {
            DefaultDialogue();
        }
    }
    void Juzgar() {
        dialogoJuzgar.SetActive(true);
    }
    private void FirstDialogue()
    {
        DisableMovements();
        if (dialogueNumber > 0)
        {
   
            dialogues[dialogueNumber - 1].SetActive(false);

        }
        if (dialogueNumber < dialogues.Count)
        {

            dialogues[dialogueNumber].SetActive(true);
            dialogueNumber++;
        }
        if (dialogueNumber >= dialogues.Count)
        {
            //TODO INTENTAR QUE SE DESABILITE EL DIÁLOGO AL DARLE CLICK Y NO POR TEMPORIZADOR
            timerOn = true;
        }
    }

    void DisableMovements()
    {
        playerController.enabled = false;
    }
    void DefaultDialogue()
    {
        StartCoroutine(DefaultDialogueCorru());
    }
    IEnumerator DefaultDialogueCorru()
    {
        //DisableMovements();
        timerOn = true;
        defaultDialogue.SetActive(true);
        yield return new WaitForSeconds(timeToLastDefaultDialogue);
        defaultDialogue.SetActive(false);
    }

    /// <summary>
    /// Guardamos la variable
    /// </summary>
    void CheckIfSpokenFirst() {
        if (gameObject.name == "Picota" && PlayerPrefs.GetInt("SpeakPikota") == 1) {
            isDialogue1 = false;
            isDefault1 = true;
        }
        if (gameObject.name == "Fresi" && PlayerPrefs.GetInt("SpeakFresa") == 1) {
            isDialogue1 = false;
            isDefault1 = true;
        }
        if (gameObject.name == "Pina" && PlayerPrefs.GetInt("SpeakPiña") == 1) {
            isDialogue1 = false;
            isDefault1 = true;
        }

        if (gameObject.name == "Peach" && PlayerPrefs.GetInt("SpeakPeach") == 1) {
            isDialogue1 = false;
            isDefault1 = true;
        }
        if (gameObject.name == "Manzana" && PlayerPrefs.GetInt("SpeakManzana") == 1) {
            isDialogue1 = false;
            isDefault1 = true;
        }
        if (gameObject.name == "Uva" && PlayerPrefs.GetInt("SpeakUvas") == 1) {
            isDialogue1 = false;
            isDefault1 = true;
        }
        if (gameObject.name == "Uva_Noche" && PlayerPrefs.GetInt("SpeakUvasNoche") == 1) {
            isDialogue1 = false;
            isDefault1 = true;
        }

        if (gameObject.name == "Pico_Noche" && PlayerPrefs.GetInt("SpeakPikotaNoche") == 1) {
            isDialogue1 = false;
            isDefault1 = true;
        }

        if (gameObject.name == "Peach_Noche" && PlayerPrefs.GetInt("SpeakPeachNoche") == 1) {
            isDialogue1 = false;
            isDefault1 = true;
        }

        if (gameObject.name == "Piña_Noche" && PlayerPrefs.GetInt("SpeakPiñaNoche") == 1) {
            isDialogue1 = false;
            isDefault1 = true;
        }
    }
    void CheckIfSpoken()
    {
        if (gameObject.name == "Picota")
        {
            if (PlayerPrefs.GetInt("SpeakPikota")==0) {
                dialogueParent.SetActive(false);
                PlayerPrefs.SetInt("SpeakPikota", 1);
                currentNpcSpoken = PlayerPrefs.GetInt("SpeakNumber");
                currentNpcSpoken += currentNpcSpokenToAdd;
                PlayerPrefs.SetInt("SpeakNumber", currentNpcSpoken);
                isDialogue1 = false;
                isDefault1 = true;
            } else {
                isDialogue1 = false;
                isDefault1 = true;
            }
        }
        if (gameObject.name == "Fresi")
        {
            if (PlayerPrefs.GetInt("SpeakFresa") == 0) {
                dialogueParent.SetActive(false);
                PlayerPrefs.SetInt("SpeakFresa", 1);
                currentNpcSpoken = PlayerPrefs.GetInt("SpeakNumber");
                currentNpcSpoken += currentNpcSpokenToAdd;
                PlayerPrefs.SetInt("SpeakNumber", currentNpcSpoken);
                isDialogue1 = false;
                isDefault1 = true;
            } else {
                isDialogue1 = false;
                isDefault1 = true;
            }
        }
        if (gameObject.name == "Pina")
        {
            if (PlayerPrefs.GetInt("SpeakPiña") == 0) {
                dialogueParent.SetActive(false);
                PlayerPrefs.SetInt("SpeakPiña", 1);
                currentNpcSpoken = PlayerPrefs.GetInt("SpeakNumber");
                currentNpcSpoken += currentNpcSpokenToAdd;
                PlayerPrefs.SetInt("SpeakNumber", currentNpcSpoken);
                isDialogue1 = false;
                isDefault1 = true;
            } else {
                isDialogue1 = false;
                isDefault1 = true;
            }
        }
        if (gameObject.name == "Peach")
        {
            if (PlayerPrefs.GetInt("SpeakPeach") == 0) {
                dialogueParent.SetActive(false);
                isDialogue1 = false;
                isDefault1 = true;
                PlayerPrefs.SetInt("SpeakPeach", 1);
                currentNpcSpoken = PlayerPrefs.GetInt("SpeakNumber");
                currentNpcSpoken += currentNpcSpokenToAdd;
                PlayerPrefs.SetInt("SpeakNumber", currentNpcSpoken);
            } else {
                isDialogue1 = false;
                isDefault1 = true;
            }
        }
        if (gameObject.name == "Manzana")
        {
            if (PlayerPrefs.GetInt("SpeakManzana") == 0) {
                dialogueParent.SetActive(false);
                isDialogue1 = false;
                isDefault1 = true;
                PlayerPrefs.SetInt("SpeakManzana", 1);
                currentNpcSpoken = PlayerPrefs.GetInt("SpeakNumber");
                currentNpcSpoken += currentNpcSpokenToAdd;
                PlayerPrefs.SetInt("SpeakNumber", currentNpcSpoken);
            } else {
                isDialogue1 = false;
                isDefault1 = true;
            }
        }
        if (gameObject.name == "Uva")
        {
            if (PlayerPrefs.GetInt("SpeakUvas") == 0) {
                dialogueParent.SetActive(false);
                PlayerPrefs.SetInt("SpeakUvas", 1);
                currentNpcSpoken = PlayerPrefs.GetInt("SpeakNumber");
                currentNpcSpoken += currentNpcSpokenToAdd;
                PlayerPrefs.SetInt("SpeakNumber", currentNpcSpoken);
                isDialogue1 = false;
                isDefault1 = true;
            } else {
                isDialogue1 = false;
                isDefault1 = true;
            }
        }
        if (gameObject.name == "Uva_Noche")
        {
            if (PlayerPrefs.GetInt("SpeakUvasNoche") == 0) {
                dialogueParent.SetActive(false);
                PlayerPrefs.SetInt("SpeakUvasNoche", 1);
                currentNpcSpoken = PlayerPrefs.GetInt("SpeakNumber");
                currentNpcSpoken += currentNpcSpokenToAdd;
                PlayerPrefs.SetInt("SpeakNumber", currentNpcSpoken);
                isDialogue1 = false;
                isDefault1 = true;
            } else {
                isDialogue1 = false;
                isDefault1 = true;
            }
            
        }
        if (gameObject.name == "Pico_Noche")
        {
            if (PlayerPrefs.GetInt("SpeakPikotaNoche") == 0) {
                dialogueParent.SetActive(false);
                PlayerPrefs.SetInt("SpeakPikotaNoche", 1);
                currentNpcSpoken = PlayerPrefs.GetInt("SpeakNumber");
                currentNpcSpoken += currentNpcSpokenToAdd;
                PlayerPrefs.SetInt("SpeakNumber", currentNpcSpoken);
                isDialogue1 = false;
                isDefault1 = true;
            } else {
                isDialogue1 = false;
                isDefault1 = true;
            }
           
        }
        if (gameObject.name == "Peach_Noche")
        {
            if (PlayerPrefs.GetInt("SpeakPeachNoche") == 0) {
                dialogueParent.SetActive(false);
                PlayerPrefs.SetInt("SpeakPeachNoche", 1);
                currentNpcSpoken = PlayerPrefs.GetInt("SpeakNumber");
                currentNpcSpoken += currentNpcSpokenToAdd;
                PlayerPrefs.SetInt("SpeakNumber", currentNpcSpoken);
                isDialogue1 = false;
                isDefault1 = true;
            } else {
                isDialogue1 = false;
                isDefault1 = true;
            }
           
        }
        if (gameObject.name == "Piña_Noche")
        {
            if (PlayerPrefs.GetInt("SpeakPiñaNoche") == 0) {
                dialogueParent.SetActive(false);
                PlayerPrefs.SetInt("SpeakPiñaNoche", 1);
                currentNpcSpoken = PlayerPrefs.GetInt("SpeakNumber");
                currentNpcSpoken += currentNpcSpokenToAdd;
                PlayerPrefs.SetInt("SpeakNumber", currentNpcSpoken);
                isDialogue1 = false;
                isDefault1 = true;
            } else {
                isDialogue1 = false;
                isDefault1 = true;
            }

        }

    }
}