using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Aldea_Spawner : MonoBehaviour
{
    [SerializeField] GameObject barGate;
    [SerializeField] GameObject casaFresiGate;
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Aldea")
        {
            PlayerPrefs.GetString("DoorToAppear");
            if (PlayerPrefs.GetString("DoorToAppear") == "CasaFresi")
            {
                gameObject.transform.position = casaFresiGate.transform.position;
            }
            if (PlayerPrefs.GetString("DoorToAppear") == "Bar")
            {
                gameObject.transform.position = barGate.transform.position;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
