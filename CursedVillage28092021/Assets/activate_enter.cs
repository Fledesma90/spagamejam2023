using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activate_enter : MonoBehaviour
{
    public GameObject textoentrar;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerController>().CompareTag("Player"))
        {
            textoentrar.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerController>().CompareTag("Player"))
        {
            textoentrar.SetActive(false);
        }
    }
}
