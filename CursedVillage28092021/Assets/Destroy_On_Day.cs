using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy_On_Day : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.GetString("DayTime");
        if (PlayerPrefs.GetString("DayTime") == "day")
        {
            Destroy(gameObject);
        }
    }
}
