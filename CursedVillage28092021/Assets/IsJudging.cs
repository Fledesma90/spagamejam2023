using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsJudging : MonoBehaviour
{
    [SerializeField] NPCdialogue nPCdialogue;
    PlayerController playerController;
    public GameObject canvasJudge;
    public GameObject canvasJudgeEng;
    // Start is called before the first frame update
    void Start()
    {
        nPCdialogue.canInteract = false;
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        nPCdialogue.isJudging = true;
        playerController.enabled = false;
        Invoke("ActivateCanvasJudge", 5);
    }

    void ActivateCanvasJudge() {
        if (PlayerPrefs.GetString("Language") == "English") {
            canvasJudgeEng.SetActive(true);
        }
        if (PlayerPrefs.GetString("Language") == "Spanish") {
            canvasJudge.SetActive(true);
        }

    }
}
