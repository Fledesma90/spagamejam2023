using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayMusic : MonoBehaviour
{
    public MusicManager musicManager;
    // Start is called before the first frame update
    void Start() {

        musicManager = Object.FindAnyObjectByType<MusicManager>();
        musicManager.trackPlaying = 2;
        //dado que no podemos acceder al método de musicManager desde este script(razones desconocidas)
        //
        if (musicManager.trackPlaying == 2) {
            AudioSource audioSource = musicManager.GetComponent<AudioSource>();
            audioSource.clip = musicManager.dayMusic;
            audioSource.Play();
        }
    }
}
