using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorial1 : MonoBehaviour
{
    public GameObject tutorial;
    [SerializeField] float timeForTutorial;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("SpeakNumber")==0) 
        {
            tutorial.SetActive(true);
        }
        
    }
    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            tutorial.SetActive(false);
        }
    }
}
