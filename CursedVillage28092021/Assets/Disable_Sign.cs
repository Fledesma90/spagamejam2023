using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disable_Sign : MonoBehaviour
{
    public GameObject sign;

    private void OnEnable()
    {
        sign.SetActive(false);
    }

    private void OnDisable()
    {
        sign.SetActive(true);
    }
}
