using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{
    [SerializeField] CanvasGroup canvasGroup;
    [SerializeField] float time;
    // Start is called before the first frame update
    void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        StartCoroutine(FadeIn());
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
    /// <summary>
    /// As long as the canvasgroup alpha is less than 0 the canvasgroup
    /// alpha is going to be substracted by Time.deltatime divided by time
    /// </summary>
    /// <returns></returns>
    IEnumerator FadeIn() {
        while (canvasGroup.alpha > 0) {
            canvasGroup.alpha -= Time.deltaTime / time;
            yield return null;
        }
        if (canvasGroup.alpha == 0) {
            gameObject.SetActive(false);
        }
    }

    public IEnumerator FadeOut() {
        while (canvasGroup.alpha < 1) {
            canvasGroup.alpha += Time.deltaTime / time;
            yield return null;
        }
    }
}
