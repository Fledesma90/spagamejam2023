using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinematicHandlerSounds : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip sonidoMujer;
    public AudioClip sonidoPajaro;
    public AudioClip sonidoTelefono;
    [SerializeField] cinematicHandler cinematicHandlerScript;
    [SerializeField] float timeParaMujer;
    [SerializeField] float timeParaPajaro;
    [SerializeField] float timeParaTelefono;
    [SerializeField] float timeParaCinematicHandler;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("SonidoMujer", timeParaMujer);
        Invoke("SonidoPajaro", timeParaPajaro);
        Invoke("SonidoTelefono", timeParaTelefono);
        Invoke("CinematicHandler", timeParaCinematicHandler);
    }

    void SonidoMujer() {
        audioSource.PlayOneShot(sonidoMujer);
    }

    void SonidoPajaro() {
        audioSource.PlayOneShot(sonidoPajaro);
    }
    void SonidoTelefono() {
        audioSource.PlayOneShot(sonidoTelefono);
    }
    void CinematicHandler() {
        cinematicHandlerScript.enabled = true;
        audioSource.Stop();
    }
}
