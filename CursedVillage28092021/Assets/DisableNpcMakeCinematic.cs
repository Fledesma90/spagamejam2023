using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableNpcMakeCinematic : MonoBehaviour
{
    npcCounter npcCounter;
    // No se podrá hacer la cinemática de denoche si hay algún personaje hablando
    void OnEnable()
    {
        npcCounter = FindObjectOfType<npcCounter>();
        npcCounter.canMakeNightCinematic = false;
    }
}
