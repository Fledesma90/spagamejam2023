using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableCanPassDialogues : MonoBehaviour
{
    [SerializeField] NPCdialogue NPCdialogue;

    private void OnEnable() {

        NPCdialogue.canPassDialogue = false;
    }


}
