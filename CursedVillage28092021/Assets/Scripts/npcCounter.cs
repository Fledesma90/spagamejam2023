using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class npcCounter : MonoBehaviour
{
    //Veremos en unity las personas con las que hemos hablado con bools
    //Esta parte es solamente informativa
    [SerializeField] bool isSpokenPikota;
    [SerializeField] bool isSpokenFresa;
    [SerializeField] bool isSpokenPiña;
    [SerializeField] bool isSpokenPeach;
    [SerializeField] bool isSpokenManzana;
    [SerializeField] bool isSpokenUvas;
    [SerializeField] bool isSpokenUvasNoche;
    [SerializeField] bool isSpokenPeachNoche;
    [SerializeField] bool isSpokenPiñaNoche;
    [SerializeField] int npcSpoken;
    [SerializeField] int maxNpc;
    //Esta bool se usará para permitir que se pueda hacer o no la cinemática
    public bool canMakeNightCinematic;
    [SerializeField] float timeToLastDialogue;
    [SerializeField] GameObject dialogueGettingLate;
    //Unity el objeto FadeImage a esto
    [SerializeField] GameObject fade;
    [SerializeField] PlayerController playerController;
    [SerializeField] Animator playerAnimator;
    string dayTime;
    //bool to disable other dialogues while it's getting night
    public bool disableOtherDialoguesWhileCinematic;
    private void Awake() {
        //lo ponemos en falso para que evitemos cargar la pantalla y que se ponga la cinemática
        canMakeNightCinematic = false;
        playerAnimator = GetComponent<Animator>();
    }
    // Update is called once per frame
    void Update()
    {
        npcSpoken = PlayerPrefs.GetInt("SpeakNumber");
        if (PlayerPrefs.GetInt("SpeakPikota") == 1) {
            isSpokenPikota = true;
        }
        if (PlayerPrefs.GetInt("SpeakFresa") == 1) {
            isSpokenFresa = true;
        }
        if (PlayerPrefs.GetInt("SpeakPiña") == 1) {
            isSpokenPiña = true;
        }
        if (PlayerPrefs.GetInt("SpeakPeach") == 1) {
            isSpokenPeach = true;
        }
        if (PlayerPrefs.GetInt("SpeakManzana") == 1) {
            isSpokenManzana = true;
        }
        if (PlayerPrefs.GetInt("SpeakUvas") == 1) {
            isSpokenUvas = true;
        }
        if (PlayerPrefs.GetInt("SpeakUvasNoche") == 1)
        {
            isSpokenUvasNoche = true;
        }
        if (PlayerPrefs.GetInt("SpeakPeachNoche") == 1)
        {
            isSpokenPeachNoche = true;
        }
        if (PlayerPrefs.GetInt("SpeakPikotaNoche") == 1)
        {
            isSpokenPeachNoche = true;
        }
        if (PlayerPrefs.GetInt("SpeakPiñaNoche") == 1)
        {
            isSpokenPiñaNoche = true;
        }
        if (canMakeNightCinematic && npcSpoken==maxNpc && PlayerPrefs.GetString("DayTime")=="day") {
            disableOtherDialoguesWhileCinematic = true;
            StartCoroutine(MakeNightCorru());
        }
        dayTime = PlayerPrefs.GetString("DayTime");
        SwitchDayTime();
    }
    /// <summary>
    /// Si no funcionase esto, entonces podríamos instanciar o activar un objeto
    /// que hiciese la cinemática de fadein y que se hiciese de noche
    /// </summary>
    /// <returns></returns>
    IEnumerator MakeNightCorru() 
    {
        PlayerPrefs.SetInt("SpeakNumber", 9);
        playerAnimator.SetBool("isWalking", false);
        playerController.enabled = false;
        dialogueGettingLate.SetActive(true);
        yield return new WaitForSeconds(timeToLastDialogue);
        canMakeNightCinematic = false;
        dialogueGettingLate.SetActive(false);
        //hacemos esto para que cuando recargue evitemos que cuando recargue la aldea
        //se spawnee el player donde no debe estar 
        PlayerPrefs.SetString("DoorToAppear", "");
        yield return new WaitForSeconds(1f);
        fade.SetActive(true);
        StartCoroutine(fade.GetComponent<Fade>().FadeOut());
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Aldea");
    }
    void SwitchDayTime() {
        if (maxNpc==npcSpoken) {
            PlayerPrefs.SetString("DayTime", "night");
        }
    }
}
