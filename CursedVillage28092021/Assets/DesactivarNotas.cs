using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesactivarNotas : MonoBehaviour
{
    [SerializeField] NotesHandler notas;
    private void OnEnable() {
        notas = FindObjectOfType<NotesHandler>();
        notas.canShowNotes = false;
    }
}
