using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
public class SoundManager : MonoBehaviour
{
    public Slider masterSlider;
    float masterSliderValue;
    public AudioMixer audioMixer;
    public static SoundManager instance;
    // Start is called before the first frame update
    void Start()
    {
        masterSliderValue = PlayerPrefs.GetFloat("MasterSliderValue");
        //ESTA LINEA HACE QUE EL SLIDER MUESTRE CUANDO CARGUEMOS EL JUEGO, POR EL SLIDER
        //EL VALOR DE LA MÚSICA
        masterSlider.value = PlayerPrefs.GetFloat("MasterSliderValue");
        audioMixer.SetFloat("Master", masterSliderValue);
    }

    public void SetMasterVolume() {
        masterSliderValue = masterSlider.value;
        audioMixer.SetFloat("Master", masterSliderValue);
    }
}
