using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuHandler : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioClip clickSound;
    public AudioClip hoverSound;

    //Panel actual en el que estamos
    public GameObject currentTab;
    [SerializeField] GameObject fade;
    private void Start() {
        //SI EMPEZAMOS EL JUEGO EN LA PANTALLA PRINCIPAL BORRAMOS LOS PLAYERPREFS
        PlayerPrefs.DeleteAll();
        audioSource = GetComponent<AudioSource>();
        //empezamos con el idioma español por defecto
        PlayerPrefs.SetString("Language", "English");
    }
    /// <summary>
    /// Method to load level 1
    /// </summary>
    public void LoadLvl1Fade() {
        fade.SetActive(true);
        StartCoroutine(fade.GetComponent<Fade>().FadeOut());
        Invoke("LoadLvl1", 3f);
    }
    public void ChangeTab(GameObject nextTab) {
        currentTab.SetActive(false);
        currentTab = nextTab;
        currentTab.SetActive(true);
    }
    /// <summary>
    /// cargaremos el nivel y borramos datos
    /// </summary>
    public void LoadLvl1() {
        //Empezamos de noche por la cinemática
        PlayerPrefs.SetString("DayTime","night");
        SceneManager.LoadScene("Cinematic");
    }

    public void SoundHover()
    {
        audioSource.PlayOneShot(hoverSound);

    }

    public void SoundClick()
    {
        audioSource.PlayOneShot(clickSound);

    }

    public void SetSpanish() {
        PlayerPrefs.SetString("Language", "Spanish");
    }
    public void SetEnglish() {
        PlayerPrefs.SetString("Language", "English");
    }
}
