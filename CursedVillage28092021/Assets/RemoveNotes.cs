using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveNotes : MonoBehaviour
{
    NotesHandler notas;
    private void OnEnable() {
        notas = FindObjectOfType<NotesHandler>();
        notas.canShowNotes = false;
        notas.enabled = false;
    }
}
