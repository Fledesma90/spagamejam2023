using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCameraSwitcher : MonoBehaviour
{
    [SerializeField] GameObject aldeaCamara;
    [SerializeField] GameObject aldeaCamaraNight;


    // Start is called before the first frame update
    void Start()
    {
        //Activamos una camara u otra dependiendo del escenario para que no se vea el SKYBOX
        if (SceneManager.GetActiveScene().name!="Aldea") {
            aldeaCamaraNight.SetActive(false);
            aldeaCamara.SetActive(false);
        }
        if (PlayerPrefs.GetString("DayTime")=="day" && SceneManager.GetActiveScene().name == "Aldea") {
            aldeaCamaraNight.SetActive(false);
            aldeaCamara.SetActive(true);
        }
        if (PlayerPrefs.GetString("DayTime") == "night" && SceneManager.GetActiveScene().name == "Aldea") {
            aldeaCamaraNight.SetActive(true);
            aldeaCamara.SetActive(false);
        }
    }

}
