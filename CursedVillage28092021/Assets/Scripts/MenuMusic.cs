using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusic : MonoBehaviour {
    public MusicManager musicManager;
    // Start is called before the first frame update
    void Start() {

        musicManager = FindObjectOfType<MusicManager>();
        musicManager.trackPlaying = 1;
        //dado que no podemos acceder al método de musicManager desde este script(razones desconocidas)
        //
        if (musicManager.trackPlaying == 1) {
            AudioSource audioSource = musicManager.GetComponent<AudioSource>();
            audioSource.clip = musicManager.mainMenuMusic;
            audioSource.Play();
        }
    }
}
