using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarNotas : MonoBehaviour
{
    [SerializeField] NotesHandler notas;
    private void OnDisable() {
        notas = FindObjectOfType<NotesHandler>();
        notas.canShowNotes = true;
    }
}
