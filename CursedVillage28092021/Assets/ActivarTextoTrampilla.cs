using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarTextoTrampilla : MonoBehaviour
{
    public GameObject textoentrar;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerController>().CompareTag("Player"))
        {
            if (PlayerPrefs.GetInt("IsKey") != 1)
            {
                textoentrar.SetActive(true);
            }
            else
            {
                textoentrar.SetActive(false);
            }
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerController>().CompareTag("Player"))
        {
            textoentrar.SetActive(false);
        }
    }
}
