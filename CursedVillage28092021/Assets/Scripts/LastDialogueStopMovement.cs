using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastDialogueStopMovement : MonoBehaviour
{
    PlayerController playerController;
 
    private void OnEnable() {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        playerController.enabled = false;
    }
    private void OnDisable() {
        playerController.enabled = true;
    }
}
