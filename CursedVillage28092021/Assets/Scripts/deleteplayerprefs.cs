using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deleteplayerprefs : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<PlayerController>()) {
            PlayerPrefs.DeleteAll();
        }
    }
}
