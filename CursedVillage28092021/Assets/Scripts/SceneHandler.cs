using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneHandler : MonoBehaviour
{
    string dayTime;
    [SerializeField] GameObject dayScene;
    [SerializeField] GameObject nightScene;
    //Poniendo las comprobaciones en el awake evitaremos bugs del tipo
    //que los objetos de dia cojan referencias de objetos de noche
    private void Awake() {
        dayTime = PlayerPrefs.GetString("DayTime");

        if (dayTime == "night") {
            nightScene.SetActive(true);
            dayScene.SetActive(false);
        }
        if (dayTime == "day") {
            nightScene.SetActive(false);
            dayScene.SetActive(true);
        }
    }

}
