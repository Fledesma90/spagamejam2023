using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsTalkingTrue : MonoBehaviour
{
    [SerializeField] NPCdialogue nPCdialogue;
    private void Start() 
    {
        nPCdialogue.isTalking = true;
    }
}
