using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementSolution : MonoBehaviour
{
    PlayerController playerController;
    // Start is called before the first frame update
    void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.enabled = true;
        Destroy(gameObject, 0.5f);
    }
}
