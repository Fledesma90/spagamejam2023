using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultDialogueNotes : MonoBehaviour
{
    NotesHandler notas;
    private void OnEnable() {
        notas = FindObjectOfType<NotesHandler>();
        notas.canShowNotes = false;
    }
    private void OnDisable() {
        notas = FindObjectOfType<NotesHandler>();
        notas.canShowNotes = true;
    }
}
