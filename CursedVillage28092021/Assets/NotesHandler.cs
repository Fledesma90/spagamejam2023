using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotesHandler : MonoBehaviour
{
    [SerializeField] GameObject notesPanel;
    [SerializeField] GameObject notificationIcon;
    //Quizas esto hay que meterlo en otro script que tenga la LibretaUI
    [SerializeField] GameObject pistaPicota;
    [SerializeField] GameObject pistaPiña;
    [SerializeField] GameObject pistaPeach;
    [SerializeField] GameObject pistaManzana;
    [SerializeField] GameObject pistaFresa;
    [SerializeField] GameObject pistaUvas;
    //int para comprobar las pistas que tenemos
    public int currentClues;
    public int cluesObtained;
    public bool canShowNotification;
    public bool canShowNotes;
    void Start()
    {
        notesPanel.SetActive(false);
        currentClues = PlayerPrefs.GetInt("SpeakNumber");
        cluesObtained =PlayerPrefs.GetInt("SpeakNumber");
        canShowNotification = true;

    }

    // Update is called once per frame
    void Update()
    {
        currentClues = PlayerPrefs.GetInt("SpeakNumber");
        if (currentClues!=cluesObtained && canShowNotification && currentClues<6) {
            notificationIcon.SetActive(true);
        } else {
            notificationIcon.SetActive(false);
        }
        if (PlayerPrefs.GetInt("SpeakPikota")==1) {
            pistaPicota.SetActive(true);
        }
        if (PlayerPrefs.GetInt("SpeakPiña") == 1) {
            pistaPiña.SetActive(true);
        }
        if (PlayerPrefs.GetInt("SpeakPeach") == 1)
        {
            pistaPeach.SetActive(true);
        }
        if (PlayerPrefs.GetInt("SpeakUvas") == 1)
        {
            pistaUvas.SetActive(true);
        }
        if (PlayerPrefs.GetInt("SpeakManzana") == 1)
        {
            pistaManzana.SetActive(true);
        }
        if (PlayerPrefs.GetInt("SpeakFresa") == 1)
        {
            pistaFresa.SetActive(true);
        }
        //si no podemos enseñar las notas también desactivaremos el panel 
        if (canShowNotes==false) {
            notesPanel.SetActive(false);
        }
        //Solo podremos comprobar la libreta si pulsamos la E y si el player no está hablando
        if (Input.GetKeyDown(KeyCode.E) && canShowNotes==true) {
            notesPanel.SetActive(!notesPanel.activeSelf);
        }
    }
}
