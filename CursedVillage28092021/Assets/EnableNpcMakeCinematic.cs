using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableNpcMakeCinematic : MonoBehaviour
{
    npcCounter npcCounter;
    private void OnEnable() {
        npcCounter = FindObjectOfType<npcCounter>();
    }
    private void OnDisable() {
        npcCounter.canMakeNightCinematic = true;
    }
}
