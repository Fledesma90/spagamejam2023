using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class cinematicHandler : MonoBehaviour
{
    //Este objeto tiene que aparecer desactivado
    public GameObject dialogo1;
    public GameObject dialogo2;
    public GameObject dialogo3;
    public GameObject dialogo4;
    public GameObject dialogo5;
    public GameObject dialogo6;
    public GameObject dialogo7;
    [SerializeField] int dialogoEnseñando;
    [SerializeField] int dialogoEnseñandoNumeroMax;
    public GameObject fadeGO;
    public Fade fade;
    public GameObject sceneChanger;
    public GameObject blackScreen;
    public GameObject blackScreenToDisappear;
    public GameObject pulsaEscape;
    //bool para poder pasar de diálogo
    public bool canPassDialogue;
    private void Start() {
        pulsaEscape.SetActive(true);
        blackScreen.SetActive(true);
        blackScreenToDisappear.SetActive(false);
        fadeGO.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {
        //comprobamos que si el diálogo enseñado es menor que 1 ó si es mayor o igual a uno y la bool de canpassdialogue es true
        if ((Input.GetButtonDown("Jump") && dialogoEnseñando<1) || (Input.GetButtonDown("Jump") && dialogoEnseñando>=1 && canPassDialogue) ) {
            dialogoEnseñando++;
        }

        if (dialogoEnseñando==1) {
            dialogo1.SetActive(true);
        }
        if (dialogoEnseñando == 2) {
            dialogo1.SetActive(false);
            dialogo2.SetActive(true);
        }
        if (dialogoEnseñando == 3) {
            dialogo1.SetActive(false);
            dialogo2.SetActive(false);
            dialogo3.SetActive(true);
        }
        if (dialogoEnseñando == 4) {
            dialogo1.SetActive(false);
            dialogo2.SetActive(false);
            dialogo3.SetActive(false);
            dialogo4.SetActive(true);
        }
        if (dialogoEnseñando == 5) {
            dialogo1.SetActive(false);
            dialogo2.SetActive(false);
            dialogo3.SetActive(false);
            dialogo4.SetActive(false);
            dialogo5.SetActive(true);
        }
        if (dialogoEnseñando == 6) {
            dialogo1.SetActive(false);
            dialogo2.SetActive(false);
            dialogo3.SetActive(false);
            dialogo4.SetActive(false);
            dialogo5.SetActive(false);
            dialogo6.SetActive(true);
        }
        if (dialogoEnseñando == 7) {
            dialogo1.SetActive(false);
            dialogo2.SetActive(false);
            dialogo3.SetActive(false);
            dialogo4.SetActive(false);
            dialogo5.SetActive(false);
            dialogo6.SetActive(true);
            dialogo7.SetActive(true);
            sceneChanger.SetActive(true);
        }
    }
}
