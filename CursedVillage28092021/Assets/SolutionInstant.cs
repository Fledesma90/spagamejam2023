using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolutionInstant : MonoBehaviour
{
    public GameObject solution;
   
    private void OnDisable() {
        Instantiate(solution);
    }
}
