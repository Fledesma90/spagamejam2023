using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiveKey : MonoBehaviour
{
    public GameObject key;

    private void OnDisable()
    {
        key.SetActive(true);
    }

}
