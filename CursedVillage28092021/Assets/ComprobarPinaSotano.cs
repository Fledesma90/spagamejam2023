using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComprobarPinaSotano : MonoBehaviour
{
    public GameObject pinaAsesina;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("PinaDestroy")==1)
        {
            pinaAsesina.SetActive(true);
        } else {
            return;
        }
    }
}
